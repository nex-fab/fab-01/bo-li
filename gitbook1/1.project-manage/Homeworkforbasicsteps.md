# The basic steps
## 1.download&installing software.<font face="" color=red size=6>
* Pic go
* Git
* Visual Studio Code

## 2.commissioning
* <font face="" color=red size=4>Pic go
  <font face="" color=black size=4>
  * Insert plug-in components
  ![](https://gitlab.com/motorcar/picgo123/uploads/f2bb3b299770b812aa0b22fa542326ce/20200325084537.png)
  * settings
   ![](https://gitlab.com/motorcar/picgo123/uploads/b29a8e1e38f1ce46a2e9060ff35e07c2/20200325084609.png)
  * New group：make sure the name of group and project without space, then copy the name into blank Group and Project in settings
  
  ![](https://gitlab.com/motorcar/picgo123/uploads/da0be4de24de0c46a9330e968719d45e/20200325084826.png)
  
   * Back Picgo. 
   * Enter the imformation.Click on "设为默认图床” and “确定”.
   * Now you can upload the pictures you need and you have two ways.
* <font face="" color=blue size=4>Git
  <font face="" color=black size=3>
  Preparation Phase
    * Create a new project and create a new file.
  ![](https://gitlab.com/motorcar/picgo123/uploads/4cdaa8adc511253a41c469bef67fbddb/20200325085326.png)
 <font face="" color=black size=3>
  * 1.Initialization
     ```
    $ cd /c/Users/95389/Documents/test
    $ git init 
    Initialized empty Git repository in C:/Users/95389/Documents/test/.git/
    ```
  * 2.Configure,setting up local identity 
    ```
    $ git config user.name "XXX"  
    $ git config user.email "XXX@xx.com":
    ```
  * 3.Clone
    ```
    $ git clone git@gitlab.com:nex-fab/guide-book.git "your url"
    ```
    ![](https://gitlab.com/pic-01/pic-liu/uploads/da275fe46be279737fbb1b4c04234c54/clone.png)

    * If Ok , we will find the following information
    ```
    wait...
    ```
    * But if we get the following
    ```
    fatal: could not create leading directories of 'git@gitlab.com:dd_ddd/test.git': Invalid argument
    ```
    <font face="" color=black size=3>  or

    ```
    fatal: Could not read from remote repository.
    Please make sure you have the correct access rightsand the repository exists.
    ```
    * The problem is SSH key，check if we have generate SSH.
    ```
    $ ls ~/.ssh/
    ```
    * There will be two result: Result1: we have the following information.
    ```
    $ id_rsa id_rsa.pub known_hosts
    ```
    * we can skip "generate SSH" Result2: no above information, we should "generate SSH"
    ```
    $ ssh-keygen -t rsa -C  "953896560@qq.com"
    Generating public/private rsa key pair.
    Enter file in which to save the key (/c/Users/95389/.ssh/id_rsa):
    /c/Users/95389/.ssh/id_rsa already exists.
    Overwrite (y/n)? y
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /c/Users/95389/.ssh/id_rsa
    Your public key has been saved in /c/Users/95389/.ssh/id_rsa.pub
    The key fingerprint is:
    SHA256:bXutx1HRQzboaK8cu4VYovxBHnaHTA/Jqr3ux8Zz1qw 953896560@qq.com
    The key's randomart image is:
    +---[RSA 3072]----+
    |              o+.|
    |          . ...oo|
    |           =o   o|
    |         .+o+.  .|
    |        S*+=.o . |
    |      . B.*ooo.  |
    |       + ==.=o+. |
    |        . o@.+oo |
    |        o=o.*E.  |
    +----[SHA256]-----+
    ```
    * Check the SSH key and copy to gitlab
    ```
     $ cat ~/.ssh/id_rsa.pub
    ```
    ```
    $ ssh-rsa AAAAB....=your email
     ```
     ![](https://gitlab.com/pic-01/pic-liu/uploads/36e345d882ddb6c9e3d94c6376de6624/settings.png)
     then
     ![](https://gitlab.com/pic-01/pic-liu/uploads/b443a907aadf217a6cc16c5104acff8c/paste_ssh.png)

    * git clone again
    ```
    $ git clone git@gitlab.com:dd_ddd/test.git
    Cloning into 'test'...
    remote: Enumerating objects: 9, done.
    remote: Counting objects: 100% (9/9), done.
    remote: Compressing objects: 100% (6/6), done.
    remote: Total 9 (delta 1), reused 0 (delta 0), pack-reused 0
    Receiving objects: 100% (9/9), done.
    Resolving deltas: 100% (1/1), done.
    ```
    * Now you can get data to your local machine
   ![](https://gitlab.com/motorcar/picgo123/uploads/be0b0ba5a6227493a0c64ac79871fea4/20200325085505.png)
    
    * Modify SUMMARY files on the gitlab————"hello world 111"
  * Command
    * git pull : download gitlab to local
    ```
    $ git pull origin master
    fatal: 'origin' does not appear to be a git repository
    fatal: Could not read from remote repository.

    Please make sure you have the correct access rights and the repository exists.
    ```
    * If you encounter the above situation.
    * solution : we need connect remote as origin
    ```
    git remote add origin "your url"
    ```
    Or
    ```
    $ git pull origin master
    fatal: refusing to merge unrelated histories
    ```
    * If you encounter the above situation.
    * solution:
    ```
    git pull origin master --allow-unrelated-histories
    ```

    ```
    $ git pull origin master
    remote: Enumerating objects: 12, done.
    remote: Counting objects: 100% (12/12), done.
    remote: Compressing objects: 100% (8/8), done.
    remote: Total 12 (delta 2), reused 0 (delta 0), pack-reused 0
    Unpacking objects: 100% (12/12), 1.24 KiB | 7.00 KiB/s, done.
    From gitlab.com:dd_ddd/test
     * branch            master     -> FETCH_HEAD
     * [new branch]      master     -> origin/master
    ```
    * Now the SUMMARY file is changed.
   ![](https://gitlab.com/motorcar/picgo123/uploads/219126f7a622ffc258b505b9e968b11e/20200325090127.png)
    * The same, we can modify the local SUMMARY to achieve the effect  SUMMARY of the website
   ![](https://gitlab.com/motorcar/picgo123/uploads/155e9d47660cbbfcd83fd8c354e221a7/20200325090146.png)
    * git push
    ```
    fatal: The current branch master has no upstream branch.
    To push the current branch and set the remote as upstream, use

    git push --set-upstream origin master
    ```
    ```
    $ git add --all
    ```
    ```
    $ git commit -m "code"
    [master 12af0f1] code
    2 files changed, 2 insertions(+), 1 deletion(-)
    create mode 160000 test
    ```
    ```
    git push origin master
    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 346 bytes | 173.00 KiB/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    To gitlab.com:dd_ddd/test.git
    3583677..12af0f1  master -> master
    ```
   ![](https://gitlab.com/motorcar/picgo123/uploads/b5577425f1cbc2d906e3c09b6239544b/20200325090252.png)
* <font face="" color=blue size=4>Visual Studio Code
<font face="" color=black size=3>
![](https://gitlab.com/motorcar/picgo123/uploads/9cde95ad7855502a4cb740dbe64e2680/20200325090329.png)
![](https://gitlab.com/motorcar/picgo123/uploads/66c18b135b572512128729253fad98a1/20200325090352.png)
![](https://gitlab.com/motorcar/picgo123/uploads/7e072c3f22259c2b41bd1e0aeca68e63/20200325090415.png)

  * make the chart

| Mould No.     | Mould life          |  cavity              | 
| ------------- |:-------------------:| :-------------------:|
| 1             | 150K                | 	2                  |
| 2             | 150K                | 4                    |
| 3             | 150K                |1                     |
| 4             | 150K                |1                     |
| 5             | 150K                | 2                    |
```
[这是baidu](www.baidu.com)
![这是图片](https://goss1.cfp.cn/creative/vcg/800/version23/VCG41649681841.jpg)
<font face="华文彩云" color=red size=5>字体是“华文彩云”，颜色是红色，字号是5
```
[nexpb](https://www.nexpcb.com/)
![图片](https://cdn2.hubspot.net/hubfs/3027780/coronabanner.png)
<font face="华文彩云" color=red size=5>字体是“华文彩云”，颜色是红色，字号是5
<font face="默认" color=blue size=4> 默认字体为黑色
