# Fusion 360 learning
## 1. Drawing 
* open fusion 360 <font face="" color=black size=4>
double clik mouse left on fusion 360 link on the laptop desk
![](https://gitlab.com/motorcar/picgo123/uploads/55ad040d494dda01f23041111ce2c178/20200328105747.png)
* new file
  ![](https://gitlab.com/motorcar/picgo123/uploads/11769f26b17cc0b4d6919b8e8d746eb5/20200328101018.png)
* right button on new file and add 2parts and rename 
![](https://gitlab.com/motorcar/picgo123/uploads/b89ef58a6f9adbe07b23a49ab0cf2308/20200328101258.png)
![](https://gitlab.com/motorcar/picgo123/uploads/c480f803a9e71602213c2b8b258f4f39/20200328101313.png)
![](https://gitlab.com/motorcar/picgo123/uploads/17f2f4a67b8ca7bd56658150bda36341/20200328101419.png)
![](https://gitlab.com/motorcar/picgo123/uploads/17f2f4a67b8ca7bd56658150bda36341/20200328101419.png)
## 2. Solid
* select the drawing and rotate to solid
![](https://gitlab.com/motorcar/picgo123/uploads/2d3196c9404c3a4c507881610b398338/20200328101829.png)
* select another drawing and stretch to solid
![](https://gitlab.com/motorcar/picgo123/uploads/cc19973e1d6305b4fff30168f5c8a73a/20200328101906.png)
![](https://gitlab.com/motorcar/picgo123/uploads/7d710abeb92c390e2a8bc604d119a886/20200328102833.png)
![](https://gitlab.com/motorcar/picgo123/uploads/3cfd6c29ef3807a6bb1b9ee5ef005aea/20200328104150.png)
## 3. Assembly
* select the assembly button
![](https://gitlab.com/motorcar/picgo123/uploads/b15e28618529c17d3166b3aa73ee2496/20200328104216.png)
* choose two parts circle and choose rotate constraint
![](https://gitlab.com/motorcar/picgo123/uploads/0a2d552285e358216ae05ac016ee58be/20200328104231.png)
* then push mouse left button and move it
* Finish