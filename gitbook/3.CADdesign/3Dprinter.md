# 3D printer
## 1. 3D printing introduce  
* background <font face="" color=black size=4>
![](https://gitlab.com/motorcar/picgo123/uploads/1239ae693702f358be0ecd4c065b1351/20200403084101.png)
![](https://gitlab.com/motorcar/picgo123/uploads/c5ee824159b28ec5e7b3b9accf5bc0dd/20200403084147.png)
* FDM&SLA
    * FDM
    ![](https://gitlab.com/motorcar/picgo123/uploads/c119dddc4dafce3de5040773fb414b35/20200403084222.png)
    * SLA
    ![](https://gitlab.com/motorcar/picgo123/uploads/1838b72230d047949e2d60fcdc83e46e/20200403084345.png)
## 2. Software Cura
* Installing software
    * download
    [website]( https://ultimaker.com/software/ultimaker-cura)
    ![](https://gitlab.com/motorcar/picgo123/uploads/fe11b607a8b6bee20550d58af7b6e35d/20200403084735.png)
    * Installing&setting
    ![](https://gitlab.com/motorcar/picgo123/uploads/0c0dd86f9893ea7011b9294a69d747af/20200403084856.png)
    according to below picture setting 
    ![](https://gitlab.com/motorcar/picgo123/uploads/0a007f17bcd40f96f83ad2fa2110af69/20200403085115.png)
    ![](https://gitlab.com/motorcar/picgo123/uploads/d80c03ad6e788c7413f8c4109432d997/20200403085521.png)

* Conversion file to .STL
    * Open UG/Proe/Solidworks/Catia/Fusion 360(For example UG)
  ![](https://gitlab.com/motorcar/picgo123/uploads/6c2d4d5efae183a10fcd66b3ecb5d1ae/20200403090024.png)
    * Save as in folder which you know
    ![](https://gitlab.com/motorcar/picgo123/uploads/16016eb9371e87907bf4a3c9f67ce4ed/20200403091400.png)
* Import file into Cura
![](https://gitlab.com/motorcar/picgo123/uploads/c35dcd94eba5cc9899b34699ae386770/20200403090409.png)
![](https://gitlab.com/motorcar/picgo123/uploads/a7f0db1523b8c6e2efc7f52f20eb3185/20200403091546.png)
adjust part size and location to blue block
![](https://gitlab.com/motorcar/picgo123/uploads/9d83f6c652bef0bfd4f973a587a76fc6/20200403091718.png)
![](https://gitlab.com/motorcar/picgo123/uploads/ac971df7b8871d568858f252863414a2/20200403091756.png)
rotate and choose a best face toward down
![](https://gitlab.com/motorcar/picgo123/uploads/b596ef71945aa17c4c31deb41841b6ce/20200403091949.png)
* adjust the parameter
![](https://gitlab.com/motorcar/picgo123/uploads/931d45c80e56ca0e498636134bd59bdd/20200403092058.png)
![](https://gitlab.com/motorcar/picgo123/uploads/6cc20009766af873dd322bedb0c6e337/20200403092118.png)
![](https://gitlab.com/motorcar/picgo123/uploads/ee1b58d7fd851eab8a048c9abf6f08f8/20200403092211.png)
![](https://gitlab.com/motorcar/picgo123/uploads/fb6c216d7f097e8a2835366a4959d7ae/20200403092233.png)
![](https://gitlab.com/motorcar/picgo123/uploads/0ff8cde0a7d1e097f311e769971149ad/20200403092305.png)
## 3. Print
* Transfer the Cura file to 3D printer and begin to print
* Postprocessing
* Finish