# Summary
## Tutorials
* [1.Project manage](https://gitlab.com/nex-fab/fab-01/bo-li)
  * [Practive](gitbook1/1.project-manage/Homeworkforbasicsteps.md)
* [2.3D software](https://gitlab.com/nex-fab/fab-01/bo-li)
  * [fusion360](gitbook1/2.3Dsoftware/fusion360.md)
* [3.CAD design](https://gitlab.com/nex-fab/fab-01/bo-li)
  * [3Dprinter](gitbook1/3.CADdesign/3Dprinter.md)
* [4. AD practice](https://gitlab.com/nex-fab/fab-01/bo-li)